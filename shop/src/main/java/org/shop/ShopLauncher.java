package org.shop;


import org.shop.api.ItemService;
import org.shop.api.OrderService;
import org.shop.api.ProductService;
import org.shop.api.ProposalService;
import org.shop.api.UserService;
import org.shop.data.Item;
import org.shop.data.Order;
import org.shop.data.Product;
import org.shop.data.Proposal;
import org.shop.repository.map.OrderMapRepository;
import org.shop.springJavaConfig.MainConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * The ShopLauncher class.
 */
public class ShopLauncher {

    private static final Logger log = LoggerFactory.getLogger(ShopLauncher.class);

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(MainConfig.class);
        ItemService itemService = context.getBean(ItemService.class);
        UserService userService = context.getBean(UserService.class);
        ProductService productService = context.getBean(ProductService.class);
        OrderService orderService = context.getBean(OrderService.class);
        ProposalService proposalService = context.getBean(ProposalService.class);
        OrderMapRepository orderMapRepository = context.getBean(OrderMapRepository.class);

        Product product = productService.getProductsByName("Kindle Fire").get(0);
        Proposal proposal = proposalService.getProposalsByProduct(product).get(0);
        orderService.createOrder(userService.getUserById((long) 2), proposal);

        log.info("Sequence: {}", orderMapRepository.getSequence());
        for (Order order : orderService.getOrdersByUserId((long) 2)) {
            log.info("Order: {}", order);
            for (Item item : itemService.getItemsByOrderId(order.getId())) {
                log.info("Item: {}", item);
            }
        }
        log.info("Product size: {}", productService.getProductSize());
    }
}
