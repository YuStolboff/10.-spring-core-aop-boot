package org.shop.springJavaConfig;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({DataInitializerConfig.class, FactoryConfig.class, RepositoryConfig.class, ServiceConfig.class})
public class MainConfig {
}
