package org.shop.springJavaConfig;

import org.shop.DataInitializer;
import org.shop.ProductInitializer;
import org.shop.ProposalInitializer;
import org.shop.SellerInitializer;
import org.shop.UserInitializer;
import org.shop.api.ProductService;
import org.shop.api.SellerService;
import org.shop.api.UserService;
import org.shop.api.impl.ProductServiceImpl;
import org.shop.api.impl.UserServiceImpl;
import org.shop.common.Sellers;
import org.shop.repository.map.ProductMapRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class DataInitializerConfig {

    @Bean(initMethod = "initData")
    public DataInitializer dataInitializer() {
        return new DataInitializer();
    }

    @Bean
    public ProductInitializer productInitializer(ProductService productService) {
        return new ProductInitializer(productService);
    }

    @Bean
    public ProposalInitializer proposalInitializer() {
        return new ProposalInitializer();
    }

    @Bean
    public SellerInitializer sellerInitializer(SellerService sellerService) {
        Map<Long, String> sellerNames = new HashMap<>();
        sellerNames.put(1L, Sellers.AMAZON);
        sellerNames.put(2L, Sellers.SAMSUNG);
        sellerNames.put(3L, "HP");
        sellerNames.put(4L, "LG");

        return new SellerInitializer(sellerService, sellerNames);
    }

    @Bean
    public UserInitializer userInitializer(UserService userService) {
        return new UserInitializer(userService);
    }
}
